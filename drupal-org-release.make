; Makefile for UA QuickStart distribution
core = 7.x
api = 2

; =====================================
; UAQS Modules
; =====================================

projects[uaqs_block_types][type] = module
projects[uaqs_block_types][subdir] = custom
projects[uaqs_block_types][download][type] = git
projects[uaqs_block_types][download][tag] = 7.x-1.0-alpha6
projects[uaqs_block_types][download][url] = https://bitbucket.org/ua_drupal/uaqs_block_types.git

projects[ua_cas][type] = module
projects[ua_cas][subdir] = custom
projects[ua_cas][download][type] = git
projects[ua_cas][download][tag] = 7.x-1.0-alpha6
projects[ua_cas][download][url] = https://bitbucket.org/ua_drupal/ua_cas.git

projects[uaqs_content_chunks][type] = module
projects[uaqs_content_chunks][subdir] = custom
projects[uaqs_content_chunks][download][type] = git
projects[uaqs_content_chunks][download][tag] = 7.x-1.0-alpha6
projects[uaqs_content_chunks][download][url] = https://bitbucket.org/ua_drupal/uaqs_content_chunks.git

projects[uaqs_core][type] = module
projects[uaqs_core][subdir] = custom
projects[uaqs_core][download][type] = git
projects[uaqs_core][download][tag] = 7.x-1.0-alpha6
projects[uaqs_core][download][url] = https://bitbucket.org/ua_drupal/uaqs_core.git

projects[uaqs_demo][type] = module
projects[uaqs_demo][subdir] = custom
projects[uaqs_demo][download][type] = git
projects[uaqs_demo][download][tag] = 7.x-1.0-alpha6
projects[uaqs_demo][download][url] = https://bitbucket.org/ua_drupal/uaqs_demo.git

projects[uaqs_event][type] = module
projects[uaqs_event][subdir] = custom
projects[uaqs_event][download][type] = git
projects[uaqs_event][download][tag] = 7.x-1.0-alpha6
projects[uaqs_event][download][url] = https://bitbucket.org/ua_drupal/uaqs_event.git

projects[uaqs_featured_content][type] = module
projects[uaqs_featured_content][subdir] = custom
projects[uaqs_featured_content][download][type] = git
projects[uaqs_featured_content][download][tag] = 7.x-1.0-alpha6
projects[uaqs_featured_content][download][url] = https://bitbucket.org/ua_drupal/uaqs_featured_content.git

projects[uaqs_fields][type] = module
projects[uaqs_fields][subdir] = custom
projects[uaqs_fields][download][type] = git
projects[uaqs_fields][download][tag] = 7.x-1.0-alpha6
projects[uaqs_fields][download][url] = https://bitbucket.org/ua_drupal/uaqs_fields.git

projects[ua_google_tag][type] = module
projects[ua_google_tag][subdir] = custom
projects[ua_google_tag][download][type] = git
projects[ua_google_tag][download][tag] = 7.x-1.0-alpha6
projects[ua_google_tag][download][url] = https://bitbucket.org/ua_drupal/ua_google_tag.git

projects[uaqs_navigation][type] = module
projects[uaqs_navigation][subdir] = custom
projects[uaqs_navigation][download][type] = git
projects[uaqs_navigation][download][tag] = 7.x-1.0-alpha6
projects[uaqs_navigation][download][url] = https://bitbucket.org/ua_drupal/uaqs_navigation.git

projects[uaqs_news][type] = module
projects[uaqs_news][subdir] = custom
projects[uaqs_news][download][type] = git
projects[uaqs_news][download][tag] = 7.x-1.0-alpha6
projects[uaqs_news][download][url] = https://bitbucket.org/ua_drupal/uaqs_news.git

projects[uaqs_page][type] = module
projects[uaqs_page][subdir] = custom
projects[uaqs_page][download][type] = git
projects[uaqs_page][download][tag] = 7.x-1.0-alpha6
projects[uaqs_page][download][url] = https://bitbucket.org/ua_drupal/uaqs_page.git

projects[uaqs_person][type] = module
projects[uaqs_person][subdir] = custom
projects[uaqs_person][download][type] = git
projects[uaqs_person][download][tag] = 7.x-1.0-alpha6
projects[uaqs_person][download][url] = https://bitbucket.org/ua_drupal/uaqs_person.git

projects[uaqs_program][type] = module
projects[uaqs_program][subdir] = custom
projects[uaqs_program][download][type] = git
projects[uaqs_program][download][tag] = 7.x-1.0-alpha6
projects[uaqs_program][download][url] = https://bitbucket.org/ua_drupal/uaqs_program.git

projects[uaqs_publication][type] = module
projects[uaqs_publication][subdir] = custom
projects[uaqs_publication][download][type] = git
projects[uaqs_publication][download][tag] = 7.x-1.0-alpha6
projects[uaqs_publication][download][url] = https://bitbucket.org/ua_drupal/uaqs_publication.git

projects[uaqs_unit][type] = module
projects[uaqs_unit][subdir] = custom
projects[uaqs_unit][download][type] = git
projects[uaqs_unit][download][tag] = 7.x-1.0-alpha6
projects[uaqs_unit][download][url] = https://bitbucket.org/ua_drupal/uaqs_unit.git


; =====================================
; UAQS Themes
; =====================================

projects[ua_zen][type] = theme
projects[ua_zen][directory_name] = ua_zen
projects[ua_zen][download][type] = git
projects[ua_zen][download][tag] = 7.x-1.0-alpha6
projects[ua_zen][download][url] = https://bitbucket.org/ua_drupal/ua_zen.git
