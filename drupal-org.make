; Makefile for UA QuickStart distribution
core = 7.x
api = 2

; =====================================
; UAQS Modules
; =====================================

projects[uaqs_block_types][type] = module
projects[uaqs_block_types][subdir] = custom
projects[uaqs_block_types][download][type] = git
projects[uaqs_block_types][download][branch] = 7.x-1.x
projects[uaqs_block_types][download][revision] = 7d1d365
projects[uaqs_block_types][download][url] = https://bitbucket.org/ua_drupal/uaqs_block_types.git

projects[ua_cas][type] = module
projects[ua_cas][subdir] = custom
projects[ua_cas][download][type] = git
projects[ua_cas][download][branch] = 7.x-1.x
projects[ua_cas][download][revision] = ad04efa
projects[ua_cas][download][url] = https://bitbucket.org/ua_drupal/ua_cas.git

projects[uaqs_content_chunks][type] = module
projects[uaqs_content_chunks][subdir] = custom
projects[uaqs_content_chunks][download][type] = git
projects[uaqs_content_chunks][download][branch] = 7.x-1.x
projects[uaqs_content_chunks][download][revision] = 13ab832
projects[uaqs_content_chunks][download][url] = https://bitbucket.org/ua_drupal/uaqs_content_chunks.git

projects[uaqs_core][type] = module
projects[uaqs_core][subdir] = custom
projects[uaqs_core][download][type] = git
projects[uaqs_core][download][branch] = 7.x-1.x
projects[uaqs_core][download][revision] = d479e1a
projects[uaqs_core][download][url] = https://bitbucket.org/ua_drupal/uaqs_core.git

projects[uaqs_demo][type] = module
projects[uaqs_demo][subdir] = custom
projects[uaqs_demo][download][type] = git
projects[uaqs_demo][download][branch] = 7.x-1.x
projects[uaqs_demo][download][revision] = 3cae9e4
projects[uaqs_demo][download][url] = https://bitbucket.org/ua_drupal/uaqs_demo.git

projects[uaqs_event][type] = module
projects[uaqs_event][subdir] = custom
projects[uaqs_event][download][type] = git
projects[uaqs_event][download][branch] = 7.x-1.x
projects[uaqs_event][download][revision] = 0a61f06
projects[uaqs_event][download][url] = https://bitbucket.org/ua_drupal/uaqs_event.git

projects[uaqs_fields][type] = module
projects[uaqs_fields][subdir] = custom
projects[uaqs_fields][download][type] = git
projects[uaqs_fields][download][branch] = 7.x-1.x
projects[uaqs_fields][download][revision] = a43e360
projects[uaqs_fields][download][url] = https://bitbucket.org/ua_drupal/uaqs_fields.git

projects[uaqs_featured_content][type] = module
projects[uaqs_featured_content][subdir] = custom
projects[uaqs_featured_content][download][type] = git
projects[uaqs_featured_content][download][branch] = 7.x-1.x
projects[uaqs_featured_content][download][revision] = c68a063
projects[uaqs_featured_content][download][url] = https://bitbucket.org/ua_drupal/uaqs_featured_content.git

projects[ua_google_tag][type] = module
projects[ua_google_tag][subdir] = custom
projects[ua_google_tag][download][type] = git
projects[ua_google_tag][download][branch] = 7.x-1.x
projects[ua_google_tag][download][revision] = cd961e3
projects[ua_google_tag][download][url] = https://bitbucket.org/ua_drupal/ua_google_tag.git

projects[uaqs_navigation][type] = module
projects[uaqs_navigation][subdir] = custom
projects[uaqs_navigation][download][type] = git
projects[uaqs_navigation][download][branch] = 7.x-1.x
projects[uaqs_navigation][download][revision] = 9a14a6c
projects[uaqs_navigation][download][url] = https://bitbucket.org/ua_drupal/uaqs_navigation.git

projects[uaqs_news][type] = module
projects[uaqs_news][subdir] = custom
projects[uaqs_news][download][type] = git
projects[uaqs_news][download][branch] = 7.x-1.x
projects[uaqs_news][download][revision] = a4041f7
projects[uaqs_news][download][url] = https://bitbucket.org/ua_drupal/uaqs_news.git

projects[uaqs_page][type] = module
projects[uaqs_page][subdir] = custom
projects[uaqs_page][download][type] = git
projects[uaqs_page][download][branch] = 7.x-1.x
projects[uaqs_page][download][revision] = fc49b21
projects[uaqs_page][download][url] = https://bitbucket.org/ua_drupal/uaqs_page.git

projects[uaqs_person][type] = module
projects[uaqs_person][subdir] = custom
projects[uaqs_person][download][type] = git
projects[uaqs_person][download][branch] = 7.x-1.x
projects[uaqs_person][download][revision] = d46bee0
projects[uaqs_person][download][url] = https://bitbucket.org/ua_drupal/uaqs_person.git

projects[uaqs_program][type] = module
projects[uaqs_program][subdir] = custom
projects[uaqs_program][download][type] = git
projects[uaqs_program][download][branch] = 7.x-1.x
projects[uaqs_program][download][revision] = a05afc0
projects[uaqs_program][download][url] = https://bitbucket.org/ua_drupal/uaqs_program.git

projects[uaqs_publication][type] = module
projects[uaqs_publication][subdir] = custom
projects[uaqs_publication][download][type] = git
projects[uaqs_publication][download][branch] = 7.x-1.x
projects[uaqs_publication][download][revision] = b697591
projects[uaqs_publication][download][url] = https://bitbucket.org/ua_drupal/uaqs_publication.git

projects[uaqs_unit][type] = module
projects[uaqs_unit][subdir] = custom
projects[uaqs_unit][download][type] = git
projects[uaqs_unit][download][branch] = 7.x-1.x
projects[uaqs_unit][download][revision] = 1a748c6
projects[uaqs_unit][download][url] = https://bitbucket.org/ua_drupal/uaqs_unit.git


; =====================================
; UAQS Themes
; =====================================

projects[ua_zen][type] = theme
projects[ua_zen][directory_name] = ua_zen
projects[ua_zen][download][type] = git
projects[ua_zen][download][branch] = 7.x-1.x
projects[ua_zen][download][revision] = 3400bfd
projects[ua_zen][download][url] = https://bitbucket.org/ua_drupal/ua_zen.git
